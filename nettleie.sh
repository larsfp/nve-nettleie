#!/bin/bash

# http://api.nve.no/doc/nettleiestatistikk/#/Nettleie/get_api_Nettleie_ValgtDatoHusholdningFritid

curl -X GET "https://nvebiapi.nve.no/api/Nettleie/ValgtDatoHusholdningFritid" -H  "accept: application/json" 

#{
#   "tariffdato": "2022-04-14T00:00:00",
#   "tariffmodell": "Energi",
#   "tariffgruppe": "Husholdning",
#   "konsesjonar": "ELVIA AS",
#   "organisasjonsnr": "980489698",
#   "fylkeNr": 30,
#   "fylkeNavn": "Viken",
#   "harMva": true,
#   "harForbruksavgift": true,
#   "periodeFraDato": "2022-01-01T00:00:00",
#   "periodeTilDato": "2022-12-31T00:00:00",
#   "fastleddEks": 1104,
#   "energileddEks": 19.15,
#   "effektleddKrMndEks": 0,
#   "effektTrinnFraKw": 0,
#   "effektTrinnTilKw": 0,
#   "omregnetArrEks": 4934,
#   "omregnetOreEks": 24.67,
#   "fastleddInkMva": 1380,
#   "energileddInkMva": 35.075,
#   "effektleddInk": 0,
#   "omregnetAarInk": 8395,
#   "omregnetOreInk": 41.975002,
#   "kvantumAar": 2020
# },
