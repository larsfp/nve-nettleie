#!/usr/bin/env python3

import requests
import json

class NVENettleie:
    # http://api.nve.no/doc/nettleiestatistikk/#/Nettleie/get_api_Nettleie_ValgtDatoHusholdningFritid

    #https://nvebiapi.nve.no/api/Nettleie/ValgtDatoHusholdningFritid?AarligTotalForbrukHusholdning=20001&EffektHusholdning=8
    api_url = "https://nvebiapi.nve.no/api/Nettleie/ValgtDatoHusholdningFritid"
    EffektHusholdning = AarligTotalForbrukHusholdning = None

    def __init__(self, EffektHusholdning=4, AarligTotalForbrukHusholdning=20000):
        self.EffektHusholdning = EffektHusholdning
        self.AarligTotalForbrukHusholdning = AarligTotalForbrukHusholdning

    def get_price(self):
        price = None # Per kWh incl. VAT

        r = requests.get(url=self.api_url,
            headers={"accept": "application/json"},
            data={
                "EffektHusholdning": str(self.EffektHusholdning),
                "AarligTotalForbrukHusholdning": str(self.AarligTotalForbrukHusholdning)
            })

        # Skipping fastledd, float(tariff["fastleddInkMva"])

        j = json.loads(r.text)
        for tariff in j:
            if tariff["tariffgruppe"] == "Husholdning" \
                and tariff["konsesjonar"] == "ELVIA AS" \
                and tariff["fylkeNavn"] == "Viken":
                # print(tariff)
                price = float(tariff["energileddInkMva"])
                break

        if price:
            return price

if __name__ == '__main__':
    n = NVENettleie(EffektHusholdning=8, AarligTotalForbrukHusholdning=25000)
    print(n.get_price())

    # {
    #   'tariffdato': '2022-04-14T00:00:00',
    #   'tariffmodell': 'Energi',
    #   'tariffgruppe': 'Husholdning',
    #   'konsesjonar': 'ELVIA AS',
    #   'organisasjonsnr': '980489698',
    #   'fylkeNr': 30,
    #   'fylkeNavn': 'Viken',
    #   'harMva': True,
    #   'harForbruksavgift': True,
    #   'periodeFraDato': '2022-01-01T00:00:00',
    #   'periodeTilDato': '2022-12-31T00:00:00',
    #   'fastleddEks': 1104,
    #   'energileddEks': 19.15,
    #   'effektleddKrMndEks': 0,
    #   'effektTrinnFraKw': 0,
    #   'effektTrinnTilKw': 0,
    #   'omregnetArrEks': 4934,
    #   'omregnetOreEks': 24.67,
    #   'fastleddInkMva': 1380,
    #   'energileddInkMva': 35.075,
    #   'effektleddInk': 0,
    #   'omregnetAarInk': 8395,
    #   'omregnetOreInk': 41.975002,
    #   'kvantumAar': 2020
    # }
    # {
    #   'tariffdato': '2022-04-14T00:00:00',
    #   'tariffmodell': 'Energi',
    #   'tariffgruppe': 'Husholdning',
    #   'konsesjonar': 'ELVIA AS',
    #   'organisasjonsnr': '980489698',
    #   'fylkeNr': 30,
    #   'fylkeNavn': 'Viken',
    #   'harMva': True,
    #   'harForbruksavgift': True,
    #   'periodeFraDato': '2022-04-01T00:00:00',
    #   'periodeTilDato': '2022-10-31T00:00:00',
    #   'fastleddEks': 3560,
    #   'energileddEks': 5,
    #   'effektleddKrMndEks': 0,
    #   'effektTrinnFraKw': 0,
    #   'effektTrinnTilKw': 0,
    #   'omregnetArrEks': 4560,
    #   'omregnetOreEks': 22.8,
    #   'fastleddInkMva': 4450,
    #   'energileddInkMva': 17.3875,
    #   'effektleddInk': 0,
    #   'omregnetAarInk': 7927.5,
    #   'omregnetOreInk': 39.6375,
    #   'kvantumAar': 2020
    # }
