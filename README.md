Fetch NVE Nettleie
==================

Fetch the price of the day from NVE's API.

The API returns more than one value, and I haven't figured out the difference between them yet.

Example use
-----------

Se bottom of code file

API
---

More info about the API:

* https://www.nve.no/om-nve/apne-data-og-api-fra-nve/
* http://api.nve.no/doc/nettleiestatistikk/#/Nettleie/get_api_Nettleie_ValgtDatoHusholdningFritid
